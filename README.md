# Deployment

[![pipeline status](https://gitlab.com/Muninn/muninn.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/Muninn/muninn.gitlab.io/-/commits/master)

Is achieved by simply making changes to the master branch and pushing them to remote.

This will automatically run the pipelines in `.gitlbab-ci.yaml`

You can then visit [StrawDDogs.co](https://strawdogs.co) to see the latest version of the site.