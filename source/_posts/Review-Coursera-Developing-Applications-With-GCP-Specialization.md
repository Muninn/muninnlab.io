---
title: 'Review: Coursera Developing Applications With GCP Specialization'
tags:
  - learning
  - cloud
  - programming
categories:
  - learning
date: 2020-12-01 20:19:27
---

{% asset_img logo_blue_rgb.png '"Coursera" "Coursera Logo"' %}

This review is coming pretty late since I completed the course last year but having found out its still an option on Coursera, and there's been a huge proliferation of Google Cloud Platform (GCP) courses since, I wanted people to know how great this one was.

You can get to the course [here](https://www.coursera.org/specializations/developing-apps-gcp).

As a "Specialisation" on Coursera that means it is part of multiple courses and Coursera offer a monthly charge for all courses. So there's an economical reason to finish the courses in good time and not laze about - as the course does allow you lose deadlines.

So I completed all the 4 courses in the 1 month suggested without any deadline extensions receiving distinction in all of them.

## Courses

{% asset_img Google_Cloud_Platform-Logo.png '"Google Cloud" "Coursera Logo"' %}

As mentioned previously the specialisation contains multiple related courses which - if you wanted - you could complete individually and pick n mix them to your hearts content. But choosing the specialisation path ensures you get a good grounding in the subject offered:

* [Google Cloud Platform Fundamentals](https://www.coursera.org/learn/gcp-fundamentals) - some of the bare essentials such as getting yourself around the cloud console itself.
* [Getting Started With Application Development](https://www.coursera.org/learn/getting-started-app-development) - starts getting into technologies such as containerisation of your applications and deployment with CI/CD.
* [Securing and Integrating Components of Your Application](https://www.coursera.org/learn/securing-integrating-components-app) - the use of the Google Cloud network and linking the various technologies together such as databases, instances and other things.
* [App Deployment Debugging and Performance](https://www.coursera.org/learn/app-deployment-debugging-performance) - the use of monitoring and logging of your application mainly through a rundown of Stackdriver.

You can get information on the individual courses [at Coursera](https://www.coursera.org/specializations/developing-apps-gcp#courses).

## Features

Free Qwiklabs access was a huge boon to this course as it allows you free access to the Google Cloud console and services for a limited time to do with as you please giving you teh opportunity to learn practically with the actual tools you would be using.

Every course in the series including the first intro course had some lab or another in [Qwiklabs](https://www.qwiklabs.com/) with some offering several. The tasks weren't massively difficult but for the price and considering it was offered alongside lectures and notes it was a great addition to the course.

Basically the courses had everything you could want: lectures, notes, documentation, forums to ask questions and finally practical lab based work. It actually felt a lot like some of my university courses and if you were dedicated and made use of it all you would get out of it what you put in.

## Criticisms

As a web developer by trade I was disappointed that a GCP course with the words "Developing Applications" in the title had absolutely ZERO development. It was all the techniques and technology needed and surrounding development on the Google Cloud platform. I don't so much as remember even doing a single "Hello World in Node or Python for one of the Qwiklabs. It was a major, major failing of the course and they should probably rename it as the course's rating of 4.6 is probably due to it lacking the development aspect and its also the reason that since doing that course I've been put off doing any Coursera course since.

Yes, its a great course. Yes, you get a ton of content. Yes, I felt happy with what I got. But NO - don't call a course "Developing Applications" then have zero development in it. It was a real slap in the face and as I came to the end of the course I wanted to ask for my money back but couldn't see any recourse to get it.

So be warned. If you want to do this course thinking you'll get the chance to develop a small cloud-based web app. Don't. You won't. It tells you about containers, pulling code, pushing it. Moving it around, monitoring it. Everything surrounding development. But no actual development.

One of the things this criticism made me realise is that the cloud is a complex place that involves a lot of work to get programs working. So much so an entire 1 month course split in 4 doesn't even have any programming in it. Its entirely dedicated to showing you the basics of getting code to the point it'll work.

## Conclusion

I've always hated it when reviews end with an indecisive comment aht could be trotted out by anyone about anything. So I aim to do the opposite.
The course was **excellent** *but* it wasn't what I expected. If you're expecting to program like the title says you won't be doing. You'll be doing everything involved in getting programs to work on the cloud.

The cloud isn't for hte faint of heart. If you want to learn cloud technology I suggest you pick one and get to know it well as they're all becoming increasingly complex as time goes on.