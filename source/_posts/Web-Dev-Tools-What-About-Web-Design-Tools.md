---
title: Web Dev Tools? What About Web Design Tools
date: 2018-07-12 17:01:51
tags:
 - web
 - code
 - programming
---

Great video from one of the devs responsible for Firefox's Dev Tools which
I now much prefer using over Chromes Dev Tools.

Check out the video below for info on the future of Dev Tools and a call to arms
for extension developers to speak up about what they'd like to API-wise to
hook into Firefox Dev Tools.

<iframe width="560" height="315" src="https://www.youtube.com/embed/oGobLmA_WVQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Some of the links to thing mentioned in the video are:

https://www.youtube.com/watch?v=oGobLmA_WVQ

* [Firefox CSS Grid Dev Tool](https://developer.mozilla.org/en-US/docs/Tools/Page_Inspector/How_to/Examine_grid_layouts)
* [Chrome Dev Tools](https://developers.google.com/web/tools/chrome-devtools/)
* [Dabblet](https://dabblet.com/)
* [Firebug (Obsolete)](https://getfirebug.com/)
* [Webflow](https://webflow.com/)
* [Local Overrides](https://developers.google.com/web/updates/2018/01/devtools#overrides)
* [Finch Developer Tools](https://chrome.google.com/webstore/detail/finch-developer-tools/phgdjnidddpccdkbedmfifceiljljgdo)
* [Grabient](https://www.grabient.com/)
* [Larsen Easing Gradients](https://larsenwork.com/easing-gradients/)
* [Axis Praxis](https://www.axis-praxis.org/)
* [Firefox Shape Path Editor](https://developer.mozilla.org/en-US/docs/Tools/Page_Inspector/How_to/Edit_CSS_shapes)
* [React Dev Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)
* [@FirefoxDevTools](https://twitter.com/firefoxdevtools?lang=en)