---
title: Best Hexo Themes 2019
date: 2019-07-28 03:05:45
tags: 
    - hexo
    - blog
---

I was about to give up on Hexo after finding new plugins I wanted to use but weren't working and stopped the generation of my blog. I narrowed it down (easily) to the ancient [Hexo Paper Box](https://github.com/sun11/hexo-theme-paperbox) theme I'd been using since the beginning.

So I did what anyone would do. Check out the [Hexo themes section](https://hexo.io/themes/index.html). Unfortunately its search feature is about all it has. No filtering based on popularity, newness or anything else.

So with a bit of Google-Fu I managed to find a great link that helped me find the [Next](https://github.com/theme-next/hexo-theme-next) theme.

Here's a few great sites that will help you get your Hexo installation get up to speed:

* **[Awesome Hexo](https://github.com/hexojs/awesome-hexo#themes)** - a site for listing some of the best Hexo plugins filtered by users.
* **[Hexo top 10 Themes](https://en.abnerchou.me/Blog/5c00ca67/)** - the top 10 themes by number of stars (this is how I found Hexo Next).

The Hexo Next theme I chose comes with a HUGE amount of customisation options for both Western and Chinese users along with theme-specific plugins you could find useful including minification, caching, etc.

THere's even theme selection for code blocks making it ideal for developer blogs.

Whats best of all though? No more errors on `hexo generate`.

Check out the [Hexo top 10 Themes](https://en.abnerchou.me/Blog/5c00ca67/) by stars and especially [Hexo Next](https://github.com/theme-next/hexo-theme-next).