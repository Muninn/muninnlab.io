title: Hexo and Other Static Site Generators on Reddit
date: 2016-02-23 23:40:34
tags:
  - hexo
  - open source
---

<img src="{% asset_path "reddit-logo.png" %}" />

If you're interested about jumping on the growing static site generator ecosystem and your a Reddit user be sure to check out these Subs for your favourite static site generator:

* [Jekyll](https://www.reddit.com/r/jekyll)
* [OctoPress Framework](https://www.reddit.com/r/OctopressFramework)
* [Hexo JS](https://www.reddit.com/r/hexojs)
* [Hugo](https://www.reddit.com/r/gohugo)
* [BrunchJS](https://www.reddit.com/r/brunchjs)
* [Middleman](https://www.reddit.com/r/middlemanapp)

There's also a main directory for all these subs being maintained at: [/r/staticgen](https://www.reddit.com/r/staticgen)

Check them out, contribute, find your favourite, get involved!
