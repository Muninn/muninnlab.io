---
title: DevOps Training - List of 5 YouTube Courses
date: 2021-02-04T20:22:19+00:00
tags:
  - devops
  - cloud
  - aws
  - azure
  - gitlab
  - list
  - links
---

<a href="https://en.wikipedia.org/wiki/DevOps">{% asset_img 2022-03-24T202546.png "Devops Logo 'DevOps Logo'" %}</a>

DevOps was invented sometime around 2008 and became hugely successful as a complement to the growing popularity of Agile. GitLab began to provide DevOps tools as part of it's free plan enabling people to access a huge multitude of free devops templates and processes to get started without the need to install specialised Jenkins installs or Kubernetes clusters.

Since 2008 the job title of devops is now [no. 10 on ITJobsWatch UK](https://www.itjobswatch.co.uk).

Below is a list of some of the best free YouTube courses I've come across while brushing up on my previously limited DevOps knowledge.

## 1. DevOps BootCamp in 14mins

TechWorld with Nana has a great reputation for providing great training materials on YouTube and although this bootcamp is just 14mins long she has plenty more in depth tutorials and information on her channel covering the subject. Well worth delving deeper after watching this quick bootcamp.

[TechWorld With Nana DevOps Bootcamp](https://www.youtube.com/watch?v=9pZ2xmsSDdo)

## 2. DevOps Master Class by John Savill

This is a great full-blown course playlist on YouTube covering everything devops from beginning to end. Each lecture is from 1-1.5hrs long and there's 10 in total so this course covers a great range of areas within devops including IaaC, Containers, Monitoring and much more.

[DevOps Master Class by John Savill](https://www.youtube.com/watch?v=R74bm8IGu2M&list=PLlVtbbG169nFr8RzQ4GIxUEznpNR53ERq)

## 3. IBM DevOps Explained

You know anything created by IBM must of a high calibre. If you're more interested in shorter (10-15min) videos over the longer lectures provided by John Savill, above, then check out the video tutorials provided by IBM. They cover other areas within devops including subjects like DevSecOps, various tools and their pros & cons.

[DevOps Explained by IBM](https://www.youtube.com/watch?v=UbtB4sMaaNM&list=PLOspHqNVtKAAm1dmyiR9WMmw1UBoOwZVj)

## 4. GitLab Specific DevOps by LevelUpTuts

If you didn't do what I did and start your foray into devops with GitLab then you may find this interesting as it's a full course dedicated to the tools and capabilities inherent in the GitLab CI/CD devops system they have available to everyone for free (some services are paid for). The free version of GitLab comes with runners for managing and deploying pipelines of various types ase don commit tags and other triggers. You're also treated to a comparison between GitLab and GitHub's version of devops in this playlist.

[DevOps GitLab by LevelUpTuts](https://www.youtube.com/watch?v=gbJUasioKiI&list=PLhf5FE9BTG6Fw0kKrzqRe_5ezYJySv-Va)

## 5. Cloud Specific Training

This 'link' is a bit of a cheat. I was originally going to link to the free courses available for each specific cloud providers devops as there's plenty of jobs out there for people familiar with devops on a particular cloud platform. However, none of them host their free courses on YouTube so I thought I'd clump all their resources under no.5. Here they are in alphabetical order:

1. [AWS Dev Ops Course](https://aws.amazon.com/training/learn-about/devops/)
2. [Azure DevOps Course](https://azure.microsoft.com/en-us/overview/devops-tutorial/#building)
3. [Google DevOps Course](https://cloud.google.com/certification/cloud-devops-engineer)