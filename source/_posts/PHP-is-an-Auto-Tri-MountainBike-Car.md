title: PHP is an Auto-Tri-MountainBike-Car
tags:
  - php
date: 2016-01-30 14:32:08
---


This is a rewrite of an old joke regarding PHP I did a while back. PHP has come a long way since but I think my description still stands today. I got the original idea after reading this: **[If Programming Languages Were Cars](http://www.cs.caltech.edu/~mvanier/hacking/rants/cars.html).

Here’s my take on PHP as a 'car':

<img src="{% asset_path "php-logo.png" %}" alt="PHP Logo" />

PHP *was* a mountian bike that got crow barred into a tricycle.  Its had a lawn mower engine strapped to its rear wheels and a turbo made from washing machine ducting kitted round the front.  To keep it all going a set of stabilisers and prayer beads have been chucked on.  The only thing that rea;lly makes it a car is that in most countries it needs a license to drive legally on the roads.

Attempts have been made to re-invent the death trap such as [HHVM](http://hhvm.com/) and [PHP7](http://www.zend.com/en/resources/php-7) but its still the same old mess under i all - *does that method have an underscore in it again? Is it haystack needle or vice versa? Oh god - check the manual we may need to jumpstart it!

> PHP will get you from A to B but its not as reliable or friendly as that Python and Ruby model that just hovered past.