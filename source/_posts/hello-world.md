title: "Hello, World"
date: 2015-05-01 14:19:13
---

Seems only right to start a new blog with this. It's far from my first having started a tech blog (remaining nameless) around about 2005/6. Not long after leaving uni. It lasted for quite a while had a lot of visitors but I got bored and so killed it off, dropped the domain, etc.

I started my previous blog when the tech world seemed to be in a state of flux. Rails hit the scene not long after Agile Development became a cool word. Django came not long after and I blogged liberally during the whole Ruby vs Python malarky. A hea;lthy dose of youthful exuberance and arrogance also added to my desire to start the blog back then.

Now I've decided to start one...well why not? It isn't the only one. I've got plans to re-launch another personal blog in the not too distant future. Very unrelated to this.

In the meantime if you wanted to know who I am read the blogs title or read the [About](/About) page.