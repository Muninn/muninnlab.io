---
title: 4 Gridsome Themes That Will Make Or Break Your Site
date: 2020-11-24 18:42:56
tags:
    - gridsome
    - javascript
    - static site generator
    - themes
---

<a href="https://gridsome.org">{% asset_img logo-normal.svg "Gridsome 'Gridsome'" %}</a>

I've recently been getting into [Gridsome](https://gridsome.org) while looking for a way to create a long planned project to create an 
<acronym title="Member of Parliament">MP</acronym> Search Service (more info on that soon hopefully with a full tutorial on setting up your Gridsome site from a REST API).

I've considered other options such as [Hexo](http://hexo.io) and [NuxtJS](https://nuxtjs.org) but I was keen to take advantage of Gridsomes claimed ability to hook into multiple content providers and API's and create a GraphQL data layer to query this data for creating pages.

Obviously when you're planning a new site you start thinking about the look & feel. 
I opted to use the [TailwindCSS plugin](https://gridsome.org/plugins/gridsome-plugin-tailwindcss) to create a very utilitarian theme.

But I thought it'd be a good idea to help anyone out there on the look out for ready made themes. 
Gridsome isn't like other static site generators so doesn't really have a theme library like Hexo but there's 
a number of great starter projects and other options you can use to get a good looking site.

## 1. [Gridsome Starters](https://gridsome.org/starters/)

Probably the best place to start as this isn't just themes its a whole site dedicated to finding the right starting project for your site. As mentioned previously Gridsome can be bent to any whim you fancy so it can work from Markdown files as easily as it can work from a new articles API.

Gridsome starters aims to help people find the right type of site first ythen gives you a simple theme to start off from there.

[Gridsome Starters](https://gridsome.org/starters/)

## 2. [JAMStack Gridsome Themes](https://jamstackthemes.dev/ssg/gridsome/)

A small list of themes provided by JAMstack who also have numerous other themes for other static site generators. Most of these themes are quite basic and some work like the starters like the above with some being built with Netlify CMS in mind or Airtable.

[JAMStack Gridsome Themes](https://jamstackthemes.dev/ssg/gridsome/)

## 3. [Gridsome Profile Starter](https://github.com/drehimself/gridsome-portfolio-starter)

This is a single theme but made for people wanting a profile site - excellant for web designers, developers or creative types.

[Gridsome Profile Starter](https://github.com/drehimself/gridsome-portfolio-starter)


## 4. [VueThemes](https://vuethemes.org)

It's worth remembering that Gridsome is a wholly VueJS framework. So anything buiult with Vue in mind is compatible with Gridsome. So this site dedicated to VueThemes is well worth a look as they can all be installed and used within your Gridsome website/app.

[VueThemes](https://vuethemes.org)

They do have [2 specifically designed for Gridsome](https://vuethemes.org/gridsome-themes) but like I say they'll all work.

It's a short list but as can be seen in the [Gridsome issues discussing themes](https://github.com/gridsome/gridsome/issues/484) 
it's not considered a priority beyond the current starter projects which provide much of the capability that themes offer.

If you know of any other themes since this post please get in touch.