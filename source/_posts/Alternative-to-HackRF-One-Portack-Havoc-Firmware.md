---
title: Alternative to HackRF One Portack Havoc Firmware
date: 2020-10-25 22:56:22
tags:
  - sdr
  - linux
  - hacking
---

Portapack HackRF One devices have become increasingly popular with them popping up in droves as clones on AliExpress and BangGood.com lately.

If you're buying one - even one claiming to be "Havoc 2 2020" edition then you'll be surprised to learn Havoc has been discontinued. Its no longer supported. Neither Havoc 1 the original or Havoc 2 which was a fork.

So if you're wanting to get the latest firmware with the greatest features incluidng many that weren't yet implemented in the 'latest' Havoc firmware then look up [Mayhem firmware](https://github.com/eried/portapack-mayhem) which you can find at the link.

It comes with many great upgrades and a very active and helpful maintainer.

At the moment its the only alternatives you can get for HackRF One Portapack editions but it is compatibe with most of the editions that have been released including touch screen versions. They also release nightlyies as well 
as numerous versions depending on what features you're portapack needs and comes with.

Be sure to check out Mayhem firmware. Its a simple copy across of a firmware.bin file to upgrade to the latest version then reset.

Mayhem Firmware: https://github.com/eried/portapack-mayhem
