---
title: Top 5 Programming Books You Need
date: 2021-08-07 21:36:11
tags:
    - books
    - programming
---

This is a simple post and a quick read but I've found the following 5 books essential to my programming career at different points.

If you're a developer of any kind these will come in handy.

## 1. Test Driven Development by Example - Kent Beck
One of the most cruicial books I've got at the moment since starting at a PCI-DSS Level 1 company where security is paramount. In the past I've been able to get away without using TDD all the time and often been discouraged from using it even at some major companies because of time constraints.

So this book is coming in very handy for my PHP development. Although the examples are in Java it's now possible to write PHP like Java with all the associated object orientation and type hinting so this is very useful from the first chapter to the last.

## 2. Extreme Programming - Kent Beck
This book is an expansion of the main core of TDD above with extra ideas covering code reviews, pair programming and other techniques to ensure programming is done at the most efficient, robust and secure way possible. The pair programming while not ideal for all companies can be good for on-boarding new devs if there's a lot of new domain knowledge required in the system.

## 3. Clean Code - Robert C Martin
This book covers practically everything from TDD to naming conventions and paths of execution in your code. Its a thicker book than the previous 2 but every once of it is well worth a read.

## 4. Pragmatic Programmer
This book is famous as being voted the number programming book in more lists than any other. Its has general advice for coders including picking a text editor, learning your tools, learning your language and being an overall better developer. No matter the tool or language used. Its a manual for becoming a better programmer overall.

## 5. Refactoring - Martin Fowler
If you've ever wondered how you can make your code better built, when best to use a specific design pattern or when to optimise and when to leave well alone. Then this book is for you. It also has the great added side-effect of making you better at code reviewing which is why its my bed time and spare-5-minute-read book. I always open up a random page, find out a nugget of gold information and learn to use it in my future projects.

## Bonus: Design Patterns - GoF
I know I should have changed the title but I have a thing for the number 5 and 3 so I needed to make it five and you get a bonus at the end: This is the original and best book when it comes to design patterns. Now you can develop PHP 7+ as if it were Java and the syntax is so similar with its culry braces and C heritage the Java examples in here can often be used with few changes to your code.

These are the book sand recommendations. Done & dusted. I needed to blog something. I'm tired of the previous ancient post.

