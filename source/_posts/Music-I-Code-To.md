---
title: Music I Code To
date: 2019-07-28 02:05:03
tags: 
    - music
    - coding
    - programming
---

It's been a LONG time since my last post so my first port of call was recovering my last post which I stupidly hadn't pushed to rmeote before my hard drive died. Then I had to play with some plugins.

Here's some of the music I like to listen to when coding to get my blood pumping and get in 'the zone':

{% soundcloud https://soundcloud.com/gabriel-14/gabriel-dresden-essential-mix-march-9th-2003 %}

Or if I'm in a Prodigy mood (which is often) then somethng like:

{% soundcloud https://soundcloud.com/ninjaerx/the-prodigy-voodoo-people %}

If I want something a bit more vocal and hip hop style then a Prodigy remix like this is all good.:

{% soundcloud https://soundcloud.com/theprodigy/method-man-release-yo-delf %}
