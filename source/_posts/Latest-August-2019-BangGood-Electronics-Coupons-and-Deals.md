---
title: Latest August 2019 BangGood Electronics Coupons and Deals
date: 2019-07-28 14:04:48
tags:
    - electronics
    - makers
    - banggood
---

As you may be aware if you've read other posts on my blog including my post on the CJMCU 8128 setup and MQ sensors usage I'm a huge fan of BangGood.com.

BangGood.com are a company that offer the cheapest electronics for makers and hobbyists I've found. Sometimes GearBest beats them but BangGood by far is the place I get my electronics and equipment from for any of my projects.

So here's a list of the latest coupons and deals from BangGood as of August 2019 (if you're reading this after this date these links usually still work!).

<h3><a target='_blank' href='https://www.banggood.com/ANENG-AN870-Auto-Range-Digital-Precision-Multimeter-19999-Counts-True-RMS-NCV-Ohmmeter-Tester-p-1268841.html?utm_campaign=BGAN870&p=3D23198123540201612N&custlinkid=431567' title='' >$29.99 for AN870 19999 Counts Digital Multimeter</a></h3>
<a target='_blank' href='https://www.banggood.com/ANENG-AN870-Auto-Range-Digital-Precision-Multimeter-19999-Counts-True-RMS-NCV-Ohmmeter-Tester-p-1268841.html?utm_campaign=BGAN870&p=3D23198123540201612N&custlinkid=431567'><img src='https://img.staticbg.com/images/oaupload/banggood/images/E8/9A/e914c912-a6b5-4ad6-a360-0c3f958434f9.jpg' alt='Multimeter' style="width:40%"></a>

<h3><a target='_blank' href='https://www.banggood.com/USB-Boost-Module-5V-to-9V12V-Step-Up-Module-Adjustable-Voltage-Current-Display-Charging-Router-Converter-p-1503884.html?utm_campaign=9V12V&p=3D23198123540201612N&custlinkid=431576' title='' >$3.99 for 5V to 9V12V Step Up Module</a></h3>
<a target='_blank' href='https://www.banggood.com/USB-Boost-Module-5V-to-9V12V-Step-Up-Module-Adjustable-Voltage-Current-Display-Charging-Router-Converter-p-1503884.html?utm_campaign=9V12V&p=3D23198123540201612N&custlinkid=431576'><img src='https://img.staticbg.com/images/oaupload/banggood/images/4C/A0/ebc49f5b-716c-4e59-aaa1-9a2565d8acb1.jpg' alt='USB Power Boost Module' style="width:40%" ></a>

<h3><a target='_blank' href='https://www.banggood.com/MINI-DS213-Digital-Storage-Oscilloscope-Portable-15MHz-Bandwidth-100MSas-Sampling-Rate-2-Analog-Channels2-Digital-Channels-3-Inch-Screen-With-Logic-Trigger-p-1412378.html?utm_campaign=BGDS213&p=3D23198123540201612N&custlinkid=431569' title='' >$142 for DS213 Mini Oscilloscope</a></h3>
<a target='_blank' href='https://www.banggood.com/MINI-DS213-Digital-Storage-Oscilloscope-Portable-15MHz-Bandwidth-100MSas-Sampling-Rate-2-Analog-Channels2-Digital-Channels-3-Inch-Screen-With-Logic-Trigger-p-1412378.html?utm_campaign=BGDS213&p=3D23198123540201612N&custlinkid=431569'><img src='https://img.staticbg.com/images/oaupload/banggood/images/FB/48/866c3be0-5626-4204-832e-e1e0bb7e787a.gif' alt='Mini DS213 Oscilloscope' style="width:40%" ></a>

<h3><a target='_blank' href='https://www.banggood.com/JDS6031-New-Hand-held-Oscilloscope-1CH-30M-200MSaS-with-USB-Charger-Probe-Cable-Set-Oscilloscope-p-1441583.html?utm_campaign=BG6031&p=3D23198123540201612N&custlinkid=431570' title='' >$72.99 for JDS6031 Hand-held Oscilloscope 30M 200MSa/S</a></h3>
<a target='_blank' href='https://www.banggood.com/JDS6031-New-Hand-held-Oscilloscope-1CH-30M-200MSaS-with-USB-Charger-Probe-Cable-Set-Oscilloscope-p-1441583.html?utm_campaign=BG6031&p=3D23198123540201612N&custlinkid=431570'><img src='https://img.staticbg.com/images/oaupload/banggood/images/B2/BD/ad0ab523-ed34-4011-bc35-cc7c7c51e1f9.jpg' alt='Hand Held Oscilloscope' style="width:40%"></a>

<h3><a target='_blank' href='https://www.banggood.com/Hantek-DSO5102P-USB-Digital-Storage-Oscilloscope-2Channels-100MHz-1GSas-p-1013032.html?utm_campaign=BG5102P&p=3D23198123540201612N&custlinkid=431571' title='' >$228 For Hantek Dso5102p Digital Storage Oscilloscope 100MHz 2 channels 1GSa/s</a></h3>
<a target='_blank' href='https://www.banggood.com/Hantek-DSO5102P-USB-Digital-Storage-Oscilloscope-2Channels-100MHz-1GSas-p-1013032.html?utm_campaign=BG5102P&p=3D23198123540201612N&custlinkid=431571'><img src='https://img.staticbg.com/images/oaupload/banggood/images/17/E2/289827a4-4f28-41ce-823b-22ecfa8e20be.jpg' alt='Hantek Professional Oscilloscope' style="width:40%" ></a>

<h3><a target='_blank' href='https://www.banggood.com/LILYGO-TTGO-T-Display-ESP32-CP2104-WiFi-bluetooth-Module-1_14-Inch-LCD-Development-Board-For-Arduino-p-1522925.html?utm_campaign=TTGO&p=3D23198123540201612N&custlinkid=431555' title='' >$8.49 for TTGO T-Display ESP32 CP2104 WiFi bluetooth Module</a></h3>
<a target='_blank' href='https://www.banggood.com/LILYGO-TTGO-T-Display-ESP32-CP2104-WiFi-bluetooth-Module-1_14-Inch-LCD-Development-Board-For-Arduino-p-1522925.html?utm_campaign=TTGO&p=3D23198123540201612N&custlinkid=431555'><img src='https://img.staticbg.com/images/oaupload/banggood/images/16/CA/808b45ee-f288-4048-a4a9-b21a5d1c7e13.jpg' alt='ESP32 TTGO with LCD Screen' style="width:40%" ></a>

<h3><a target='_blank' href='https://www.banggood.com/DANIU-ADS5012H-Digital-2_4-inch-TFT-Screen-Anti-burn-Oscilloscope-500MSs-Sampling-Rate-100MHz-Analog-Bandwidth-Support-Waveform-Storage-and-Built-in-Large-3000mah-Capacity-Lithium-Battery-p-1469066.html?utm_campaign=BG5012H&p=3D23198123540201612N&custlinkid=431572' title='' >$69 for DANIU ADS5012H Digital 2.4-inch TFT Screen Anti-burn Oscilloscope 500MS/s Sampling Rate</a></h3>
<a target='_blank' href='https://www.banggood.com/DANIU-ADS5012H-Digital-2_4-inch-TFT-Screen-Anti-burn-Oscilloscope-500MSs-Sampling-Rate-100MHz-Analog-Bandwidth-Support-Waveform-Storage-and-Built-in-Large-3000mah-Capacity-Lithium-Battery-p-1469066.html?utm_campaign=BG5012H&p=3D23198123540201612N&custlinkid=431572'><img src='https://img.staticbg.com/images/oaupload/banggood/images/A4/80/c52cc0df-17b4-4256-bee1-3d660de06ebc.gif' style="width:40%" alt='Hand Held Oscilloscope' ></a>

<h3><a target='_blank' href='https://www.banggood.com/Handskit-5V-8W-Solder-Iron-Wireless-Charging-Soldering-Iron-Mini-Portable-Rechargeable-Battery-Soldering-Iron-with-USB-Welding-Tools-p-1490687.html?utm_campaign=BG4824ac&p=3D23198123540201612N&custlinkid=431574' title='' >$11.11 for Handskit Battery Soldering Iron</a></h3>
<a target='_blank' href='https://www.banggood.com/Handskit-5V-8W-Solder-Iron-Wireless-Charging-Soldering-Iron-Mini-Portable-Rechargeable-Battery-Soldering-Iron-with-USB-Welding-Tools-p-1490687.html?utm_campaign=BG4824ac&p=3D23198123540201612N&custlinkid=431574'><img src='https://img.staticbg.com/images/oaupload/banggood/images/9A/08/8b126cc4-799d-4b20-a252-f1847f53b021.jpg' alt='Battery Operated Soldering Iron' style="width:40%" ></a>

If you'd like me to make this a regular thing let me know. I don't just make this post for the hell of it. I just used one of the links to get myself this [USB soldering iron](https://www.banggood.com/MINI-TS80-Digital-OLED-USB-Type-C-Programable-Soldering-Iron-Station-Solder-Tool-Built-in-STM32-Chip-p-1330060.html?utm_campaign=BGTS80&p=3D23198123540201612N&custlinkid=431553) which I originally lost a few weeks back.

Dogfooding it as they say.

Anyway - hope someone finds these useful and leave comment below if you want more of these offers.