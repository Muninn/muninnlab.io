---
title: Programmers Xmas Annual Leave Plan
date: 2020-12-24 14:15:15
categories: personal
tags:
  - personal
  - history
  - PHP5
---

The blog for this plan was one I had well into the Xmas Annual Leave but thankfully I already had a pretty good idea of my plans already before the Leave started.

It helped that I had my probation period meeting (which I passed btw) with some recommendations shortly before the beginning of my Annual Leave.

In the criticisms of my initial 3 months it was made clear I could have been faster to learn legacy code but that my code quality was good and various other aspects. I want to focus on what could be improved here as it basically decided what I would be doing with my holidah and continue to do as the holiday progresses.

I'm Buddhist and have never been very fond of Christmas anyway. I also live alone so see no special reason or have any special desire to treat the day or holidays any differently than I do others. I do and always have seen Annual Leave when not abroad as an opportunity to work on my own projects. Whether they be my home network of Raspberry Pis and their clones or Home Assistant and its modules. However this year I decided after being told I need to 'improve my legacy code pick up' that this holiday's plan is:

## The Plan

1. Identify all open source packages I offer via [Composer](https://getcomposer.org), [NPM](https://www.npmjs.com) or [PyPi](https://pypi.org) - my 3 main languages. I've already started [EU Tyre Label](https://github.com/OdinsHat/eu-tyre-label-generator), [Clickatell Yii Plugin](https://github.com/OdinsHat/yii-clickatell-smscomponent) (likely to be converted into a standard composer package), [AWIN Feeder for Wordpress](https://github.com/OdinsHat/AWIN-Feeder-Wordpress-Plugin), [Hexo Tag Deezer](https://github.com/OdinsHat/hexo-tag-deezer), etc.
2. See how far behind they are and if they are *legacy* code I'd create a new repo or branch to bring the old code up to speed **(practice makes perfect)**.
3. Find dead projects others have abandoned on Github or one of the package indexes above to resurrect and polish them off to support the version I use at work. E.g. PHP5.4 library upgrade to PHP7.3+ library (maybe create a PHP8 lib while I'm at it). If it uses a technology from work I don't know (Redis, SNS, Queues) then all the better - two birds one stone. 
4. Update Github badges ([Appveyor](https://github.com/marketplace/appveyor), [CircleCI](https://github.com/marketplace/circleci), TravisCI) with the latest build actions and paths so they have more accurate and up-to-date CI/CD pipelines.
5. Update my LinkedIn profile as I no longer have to be defined by my horrible time at XBite Chesterfield, which really knocked my confidence.
6. Use [PHPStorm](https://www.jetbrains.com/phpstorm/) instead of my usual [VSCode](https://code.visualstudio.com). I can use the 30 day evaluation which is more than enough time to make use of it during the holiday period. I've been wrestling between which program to use since I started at this job but I think either way I never properly got to know PHPStorm and all its shortcuts and power user features.
7. Use my **Big List of Ideas** I keep in [Todoist](https://todoist.com) to create new projects or find old, existing projects that are similar but need some work to get some regular coding practice.

The above activities should make me better at picking up old code and bringing it up to speed with the latest version - exactly what I didn't do well at probation. Thereby making this a holiday that benefits me because I get a break (coding is a hobby as well as work) and work gets a better coder.

Everybody wins.