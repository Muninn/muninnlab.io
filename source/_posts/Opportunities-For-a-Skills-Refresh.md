title: Opportunities For a Skills Refresh
date: 2017-08-31 17:25:57
tags:
  - programming
  - learning
  - resources
---

Although some of my recent projects have provided the opportunity stretch my muscles in Laravel, Google API integration, Google Apps Scripting and various other areas I felt it was time for a more structured and comprehensive review of my techgnical skills. Thats one point I will make clear here is I'm focusing on technical skillset and not others like <acronym title-="Project Managament e.g. SCRUM">PM</acronym> or marketing. Those will be part of another post or time as I've had plenty of experience in those and they by their nature move slower in development and change than the webs technologies.

# So where to go to go to boost your web skills in 2017?

I've already heard of the common skills game websiutes such as [Code Wars](https://www.codewars.com) but I also wanted to take the chance to learn some different but related areas. Maybe in cloud computing and definitely motre in and area I've felt lacking in a while and thats frontend JS frameworks.

A good introductory place for heklping to compare some of the places I mention below is a new-wishg site: [Slant](https://www.slant.co) which is great for finding out crowd sourced opinions on various technical subjects.

## Google CodeLabs

{% asset_img google-codelabs.png [Google CodeLabs Screenshot] %}

* **FREE** programming resource!
* Covers a narrow selection of the latest technologies being pushed by Google
* [Link](https://codelabs.developers.google.com/)

Absolutely my favourite in the list as it covers a lot of new techology Google is trying to push such as Progressive Web Apps, Accelerated Mobile Pages, Pushg Notifications to web apps, Web Assembly and much much more. Not only that it's all free and really great quality with step by step video guides to c9ompleting each project.

They also cover a huge range of categories including [Web](https://codelabs.developers.google.com/?cat=Web), <acronym title="Internet of Things">IoT</acronym>, [Search](https://codelabs.developers.google.com/?cat=Search), [Cloud](https://codelabs.developers.google.com/?cat=Cloud), [Geo](https://codelabs.developers.google.com/?cat=Geo) and many more. All with duration estimates and a finished product. From there you can obvuiously adapt it or use the techniques on your existing projects.

For example [a Service Worker codelab](https://codelabs.developers.google.com/codelabs/offline/index.html?index=..%2F..%2Findex) is helping me to speed up some of our Bridgestone sites adding modern Progressive features.

One thing I found lacking in Google CodeLabs is a **lack of depth**. For example I wanted to create a 3 page Polymer 2.0 app using the 
Corousel tutorial. It was only one page of a 3 page app but evenb this small tutorial had me stumped at the firts hurdle. Nothing was styling right, things weren't iubncludiung correctly. It turns out I needed a beginners course in Polymer. So I dove into one of the other courses butr again hit the same hurdle. Even the ones titled something like _"from scratch"_ didnm't help much more.

They all seemed to expect some semblence of knowledge for every tutoriual which left me feeling a little stupid and moved onto other area.

**CodeLabs are great for scrubbing up and polishing yourexisting knowledge but they aren't great for learning new techniques.**

## Cloud Academy

{% asset_img cloud-academy.png [Cloud Academy Screenshot] %}

* **7 day free trial** to the courses
* **from $59 p/m**
* [Link](https://www.cloudacademy.com) 

This service cost is a little steeper especially cosnidetring it only covers Cloud Computing but but it does scratch that itch of doing something different. It offers a large library of courses into cloud computing generally as well as specific course pathways that cover one of the the 3 major cloug computing platforms AWS, GCP or Azure.

I cancelled my Cloud Academy free trial pretty quick as I noticed Azure was offering a free Azure course at PluralSight and the cost for the relkatively narrow focus just couldn't be justified. Maybe if I was focusing on a complete career change in the future I could imagine it but for now it seems a bit OTT.

## PluralSight

{% asset_img pluralsight.png [PluralSight Screenshot] %}

* **10 day free trial** to the courses
* **from $29 p/m**
* [Link](https://www.pluralsight.com/)

There's so many things I love about this service that I've seen grow over time. As I write this blog post I'm wondering why haven't I signed up for this fully already. I've had a few free sign up periods but never kept my subscription for some reason or another.  But the sheer numbver of coutrses on everything from tech to cloud to project management is insane and it truly sells itself. I swear I dont' rememebr PM being one of the learning courses on here before but it is [now](https://www.pluralsight.com/search?q=project%20management).

On top of that yoyu can now monitor what you know and don't know. Find skill gaps you need to fill. Its other main competitors don't really touch it (Team Treehouse & Code Academy are both for simpler concepts).

They have online interactive technical labs where you can try out what you're learning as well as a mobile app to watch the videos that go with the courses.

I'd probably recommend this over Cloud Academy first and do Cloud Academy more as a specialisation at a later date because this does have some [cloud-based courses](https://www.pluralsight.com/search?q=azure).

## Others

There's tons more I haven't mentioned in this roiundup as I've found I'm starting to be paralised by choice. I just need to sit down and pursue one or two and complete the courses otherwise none will get done.

I may do anoter list of these in the future including CodeSchools, CodeAcademy, EggHeads, StackSkills, etc.