---
title: 25 Essential MySQL Resources
tags:
  - mysql
  - lists
  - security
  - databases
  - sysadmin
date: 2020-12-02 21:24:17
---

{% asset_img "pull-left" mysql_PNG23.png 300 300 '"MySQL Logo" "MySQL Logo"' %}

Anyone who’s spent more than 5 minutes on the MySQL documentation will realise it leaves a lot to be desired.  The same goes for many of the 3rd party apps latched onto this platform which offer mediocre functionality and anything beyind the basic comes at a huge premium.

Well here’s a set of 20 resources, tools, articles that hope to break that and bring it all under one roof.  Enjoy.

## Design & Administration Tools

* [Adminier](https://www.adminer.org/) - A really simple PHPMyAdmin alternative. I find it useful as a basic database administration panel during development of new web-based projects.
* [MyDB Studio](http://www.mydb-studio.com/) - Makes a useful administration app for MySQL and also has some design capabilities such as the SELECT wizard to help with overly complicated queries.
* [HeidiSql](http://www.heidisql.com/) - Provides an excellent GUI for administrating you MySQL database and can be used in Linux via WINE.
* [AutoMySQLBackup](https://github.com/sixhop/AutoMySQLBackup) - Why go to the special effort of producing scripts to backup when you can use this.
* [PHPMyAdmin](https://www.phpmyadmin.net/) - Been around for years and still going strong.  Its not the most speedy when handling large sets of data (try offline apps such as SQLYog or MyDB) but its got every functionality you can shake a stick at.
* [DBeaver](https://dbeaver.io/) - Similar to HeidiSQL but with better features and works on multiple platforms including Linux.

## Security Tools

[SQLIer](https://github.com/AsdFinal/sqlier) - A SQL injection tool which you provide a URL to and it does all it can to perform SQL injection. You'll often find this installed by default on security distros like Kali or ParrotOS Security.
[SQLMap](http://sqlmap.org/) - A blind SQL injection tool thats a veteran with a plethora of features that makes SQLIer look like a Hello World app in comparison. It provides support for PostgreSQL, MSSQL as well as MySQL. Absolutely essential for people performing pen testing against a server. Will often be installed by default on Kali Linux, Black Arch, ParrotOD, etc.
[Absinthe](https://github.com/cameronhotchkies/Absinthe) - An application which is available on Linux and Windows.  It provides blind MySQL injection brute forcing but with more features than SQLIer.
[SQID](http://sqid.rubyforge.org‘) - SQL Injection Digger was created in Ruby as a command-line tool for brute force SQL injection testing. Can scrape Googl for potential targets as well as using Tor as a means to hide the identity of the attacker. More of a black hat tool than some of the others here.

## Optimising MySQL

[MySQL Performace Blog](https://www.percona.com/blog/) - By the masters of high performance MySQL at Percona.
[MySQL Percona Toolkit](https://www.percona.com/software/database-tools/percona-toolkit) - a ton of tools to help with tweaking your MySQL database for the best performance.
[101 Tips on MySQL Perormance](https://www.monitis.com/blog/101-tips-to-mysql-tuning-and-optimization/) - a 2019 blog post with a lot packed into a small space.
[Exclusive MySQL Performance Tuning Tips](https://www.cloudways.com/blog/mysql-performance-tuning/) - They just keep on trucking with those optimisation tips.

## Knowledge

[SQLZoo](https://www.sqlzoo.net/) - The basics of SQL.
[10 Essential MySQL Performance Tips](https://www.infoworld.com/article/3210905/10-essential-performance-tips-for-mysql.html) - some basic tips on keeping your MySQL database performing at max.
[MySQL Cheat Sheet](https://devhints.io/mysql) - Provided by devhints.io.
[10 Common Mistakes of MySQL Design](https://www.red-gate.com/simple-talk/sql/database-administration/ten-common-database-design-mistakes/) - Learn what not to do!
[MySQL Clustering How-To](https://www.digitalocean.com/community/tutorials/how-to-create-a-multi-node-mysql-cluster-on-ubuntu-18-04) - Digital Ocean is a great resource for documentation and tutorials. They've also startedd making some pretty good in-roads into providing simple cloud hosting.
[MySQL Master-to-Master Replication](https://www.digitalocean.com/community/tutorials/how-to-set-up-mysql-master-master-replication) - Another great Digital Ocean tutorial but this time covering master-master replication.
[MySQL vs. PostgreSQL](https://www.xplenty.com/blog/postgresql-vs-mysql-which-one-is-better-for-your-use-case/) - One of **many** comparisons of the 2 major RDBMS databases.

## Books

I guess these could go under knowledge but these you have to buy so they get their own section.

* [MySQL Pocket Reference](https://amzn.to/2Jxrj5d) - the definitive O'Reilly pocket reference for MySQL.
* [High Performance MySQL: Optimization, Backups, and Replication](https://amzn.to/3lprNrv) - another great book by O'Reilly but focusing on performance tuning your database.
* [MySQL 8 Performance Tuning](https://amzn.to/2HWKvJ8) - another book dedicated to tuning every bit of performance from your database that you can.
* [Designing Data-Intensive Applications](https://amzn.to/33x8bva) - how to build applications form the ground up with performance in mind when using a backend database.

If you think I’ve missed anything off then leave a note in the comments and I’ll make the list grow.