title: 'No Need to Download Sysinternals Tools'
date: 2016-03-24 05:46:23
tags:
 - windows
 - sysadmin
---

If you’re a Windows user then you’ve likely come across [Sysinternals](https://technet.microsoft.com/en-gb/sysinternals/bb545021.aspx?f=255&MSPPError=-2147217396) at some point during your time. Although a lot of people may know of it not everyone uses it to its maximum potential.

If you’ve ever been in a position with a unexplainable BSOD, unhelpful error dialogs, then check out Mark Russinovich’s “The Case of the Unexplained” [webcasts](https://channel9.msdn.com/events/ignite/2015/brk3316) where he’ll give tips on figuring out what dialogs like these actually mean:

<img src="{% asset_path "olxerr.gif" %}" alt="Outlook Express Dialog" />

Anyway you don’t actually need to download the tools as they have been made available as a share at `\\live.sysinternals.com\Tools\`

So you can add them as a network drive for easy access
