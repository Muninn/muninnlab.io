---
title: 15 IoT Dashboards To Display Your Data
date: 2018-05-01 16:58:34
tags:
 - esp8266
 - esp32
 - arduino
 - iot
 - mqtt
 - internet of things
---

## Why?

Eveyone loves a list and this list covers IoT dashboard and services for hackers and makers that can use it for their IoT devices such as their ESP8266 or sensor connected Raspberry Pi.

## 15 IoT Dashboards With Links

1. [Thingsbord.io](https://thingsboard.io/) - no prices displayed which is always a red flag for me but this is a self install system anyway. A bit like Gitlab as opposed to GitHub. One thing they offer is a [Community Edition](https://thingsboard.io/docs/user-guide/install/installation-options/) (free) and a [Professional Edition](https://thingsboard.io/products/thingsboard-pe/) with value added features (paid). As an installable service this would most likely end up on one of my home mini computers such as the Banana or Orange Pi. 
2. [Thinger.io](https://thinger.io/pricing/) - a not-so-generous free edition allows you to hook up 2 devices for free so you can at least get a very basic feel of what a dashboard of sensors in your home would look like. To be fair they do have a more reasonable Maker price range for just $3.95 at time of writing which enables connecting up to 20 devices. But if you check out my next article on the sorts of data you can collate then you will soon fill that quota and be needing the moire expensive range.
3. [Samsung ARTIK](https://artik.cloud/pricing/) - I didn't havre greatr success with this the first time I tried using it with my Amazon Alexa Echo Plus which seeemed capable of detecting a few YeeLights then doing nothing with them. But since then I've decided to take another look and they seem to offer a generous package. 100k messages (sensor updates) per month and 30 day retention. The retention period is shortish but it seems pretty good from the outside and I look forward to making this one of the services I check out first. 
3. [Freeboard.io](https://freeboard.io) - **NOT actually FREE!**. They have prices ranbging from $12 up and I have no idea why they keep their existing name because I don't even see a way of downloading a community edition for local install. This is definitely a service I wouldn't recommend or use.
4. [TheThings.io](https://thethings.io/) - A service that likes the idea of hiding pricing and offering free trials and free this and free that then right at the bottom "Pricing"....$29 p/m for the lowest offering.
<blockquote class="pull-right">
 I really wish services wouldn't hde their pricing. I don't care how good a service is - if it isn't straight and upfront with its pricing I don't care how good it is - I'm not interested.
</blockquote>
5. [Maniflux](https://www.mainflux.com) - A free downloadable and also hostable solution that seems to have some great features. It's a shame its not as popular as some of the other snazzier platforms out there and if you wanted to get involved in the development of it checkout their [GitHub](https://github.com/mainflux/mainflux)

## The Others to Follow Shortly

Due to unforseen circumstances the rest will follow in a day or two. 

