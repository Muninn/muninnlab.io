title: Are Older Programmers Still Viable?
date: 2016-02-24 13:25:20
tags:
 - programming
---

Found this interesting video on whether older programmers can still be relevant. A good question considering I'm now in my thirties and Mark Zuckerberg famously said:

> "If you're over 30, you're a slow old man" ~ Mark Zuckerberg

*(at the time of writing Mark Zuckerberg is 32yrs old - the same age as me)*

Along with a string of other similar statements from Zuck and major people at Intel, etc.

I wonder if Zuckerberg's opinion has changed since he reached 30?

Anyway on to the video which I think hits the nail on the head:

<iframe title="Are Older Programmers Still Viable Video by an older programmer" width="560" height="315" src="https://www.youtube.com/embed/hUfbfA481qQ" frameborder="0" allowfullscreen></iframe>

**tldw;** Basically if you don't keep up with the trends you will become a dinosaur. But that goes for everyone in every industry.

Thankfully I'm pretty comfortable embracing new technologies! In fact I'm obsessed with checking out new tech and that obsession doesn't seem to be abating anytime soon.
