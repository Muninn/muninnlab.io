---
title: Vue Experiments - How I'm Learning Vue My Arduino Way
date: 2018-05-16 13:06:35
tags:
 - vuejs
 - javascript
---

<img src="{% asset_path vuejs-logo.jpg %}" alt="VueJS 2.0 Logo" />

In a previous post I mentioned how I'd setup a repo called ["arduino-experiments"](https://github.com/OdinsHat/arduino-experiments) to learn [Arduino](http://www.arduino.cc) better and eventually to move onto ESP32 and ESP8266 chips. [The repo](https://github.com/OdinsHat/arduino-experiments) was set up with a directory structure with a general set of rules and I'd have all my experiments in different directories. With each experiment getting more and more difficult. As this has gone on I've learnt far better, easier and faster with practical examples and its given me much more focus and motivation in my learning. Even producing actual production items such as my LCD displaying temp/hum sensor unit.

So I decided *why not do the same for VueJS*

I've tried learning VueJS a few times and failed miserably due to having little structure and using multiple courses with no solid line guiding me., But as I'd found success in thre Arduino way I decided to buckle up and use the same method.

<img src="{% asset_path GitLab_Logo.svg.png %}" alt="Gitlab Logo" style="height:10em" />

### So say hello to [VueJS2.0 Experiments](https://gitlab.com/Muninn/vuejs-2-experiments)

This repo is hosted on [Gitlab](http://www.gitlab.com]) *(my preffered choice for work and 'proper' projects due to its free nature and CI/CD features)*.

You can check out the repo and whats going on at any time at the link above.

My first port of call with this has been to use a [Udemy course by Maximilian Schwarzmüller](http://tidd.ly/106e16c7) which has a 4.7 rating and if not at time of blog I've picked up for $10. Its a fantastically detailed look into Vue and I've started it form the beginning and I'm just getting into doing the basic getting it working in JSFiddle and moving onto doing full Single Page apps late rin the course.