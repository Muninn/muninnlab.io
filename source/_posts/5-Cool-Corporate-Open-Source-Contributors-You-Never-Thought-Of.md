title: "5 Cool Corporate Open Source Contributors You Never Thought Of"
date: 2015-05-25 09:58:39
tags: 
  - open source
  - javascript
---

I had the idea for this post after accidentally stumbling across [KrakenJS](http://krakenjs.com/) and realising its a Paypal open source project. 

OK so its pretty obvious companies like Google, RedHat, Oracle, etc all contribute to the open source community and even stand on its shoulders to a great extent. But after finding Paypal's contributions I did some digging to find some lesser known coroporate open source contributors. And some cool projects popped up.

There was a time when I'd do a lot of coding using Google API's and open source libraries but over the years I'd become dissillusioned with them and as a result subconciously steered clear of any future corporately sponsored projects.

### Quick KrakenJS intro before the main list
Not only is KrakenJS a much beefed up version of the Node framework ExpressJS with lads of extras including:

* Application Security
* NPM Proxy
* Dust I18N
* Dust Context

It's also a way to structure your project. It is like [Sinatra's](http://www.sinatrarb.com/) [Padrino](http://www.padrinorb.com/) of the Ruby world.

## 1. Paypal

<img src="{% asset_path "paypal-logo.png" %}" />

As I've already mentioned Paypal's greatest contribution (<acrnym title="In My Humble Opinion">IMHO</acronym>) KrakenJS here's **[Paypals GitHub page with all their projects](http://paypal.github.io/)**:

* [KrakenJS](http://krakenjs.com/) - As above.
* [NemoJS](http://paypal.github.io/nemo/) - Selenium web driver.
* [SeLion](http://selion.io/) - Java test automation
* [HTML5 Video Player](http://paypal.github.io/accessible-html5-video-player/) - WTF?! (unexpected)
* [AATT](http://github.com/paypal/AATT/) - Automated Accessiblity Testing Tool
* [Bootstrap Accessibility Plugin](http://paypal.github.io/bootstrap-accessibility-plugin/)
* [Paypal SDKs](http://paypal.github.io/sdk/) - for accessing their platform via API.

## 2. Netflix
First it was a major banking organisation. Now we've the darling of media streaming services to the public and paraiah to US ISPs.

<img src="{% asset_path "netflix-logo.png" %}" />

The Netflix open source contribution is no less impressive than PayPals - maybe even more so which is why its criminal its not more widely known. So here's a heads up on their:

* [Tech Blog](http://techblog.netflix.com/)
* [Git Hub Repos](https://github.com/Netflix)

Some of their notable projects:

* [Asgard](https://github.com/Netflix/asgard) - Web interface for application deployments and cloud management in Amazon Web Services (AWS)
* [Scumblr](https://github.com/Netflix/Scumblr) - cool custom search engine. Check the page for full info.
* [SimianArmy](https://github.com/Netflix/SimianArmy) - Tools for keeping your cloud operating in top form.


## 3. Spotify

* [Developer Blog](https://developer.spotify.com/)
* [GitHub Repos](https://github.com/spotify)

Some of their projects:
* [Luigi](https://github.com/spotify/luigi) - Flexible workflow and data piping library written in Python. Great docs for this project too and even has [built in workflow visualisation](https://raw.githubusercontent.com/spotify/luigi/master/doc/user_recs.png).
* [Puppet Explorer](https://github.com/spotify/puppetexplorer) - Puppet web interface written in CoffeeScript using AngularJS.

Check out the developer blog for all their API's and SDKs.

## 4. UK Government

<img src="{% asset_path "hm_government.png" %}" />

What? That stuffy old wood and leather palace with its yays and nays? Yep - ineed they have such a prolific open source collection on Github (19 pages of repos!) It was hard to dig out the gems. Its worth checking them all and there's some that are criminally under-appreciated. Here's a few:

* [Magna Charter](https://github.com/alphagov/magna-charta) - Accessible, useful, beautiful barcharts from HTML tables.
* [Gov.UK Styleguides](https://github.com/alphagov/styleguides) - Good to see the government have style guides in place.
* [Router](https://github.com/alphagov/router) - Just, erm. Wow. Check the readme - maybe you'll make more sense of it than me. Seems built on Go and handles routing for the main Gov.uk site.  

One thing I have noticed is the UK Government does seem to like it's Ruby, esp. Ruby on Rails.

Check out the full 19 page monster list at [AlphaGov](https://github.com/alphagov/).

## 5. The Guardian
The UK newspaper currently has a list of repos to rival the Government's but the potential for cool finds does seem a bit higher. Here's a few I managed to dig out:

* [Prout](https://github.com/guardian/prout) - Looks after your pull requests, tells you when they're live.
* [Alerta](https://github.com/guardian/alerta)
* [HTML Janitor](https://github.com/guardian/html-janitor) - santises HTML.
* [Guss Typography](https://github.com/guardian/guss-typography) - Next Gen type scale and other Sass typography helpers

Of course the Guardian wouldn't be much of a newspaper without its own developer blog now would it:

* [Guardian Developer Blog](http://www.theguardian.com/info/developer-blog)
* [Guardian Github Account](https://github.com/guardian)

If you think I've missed off any great projects let me know: doug (at) tintophat (dot) com