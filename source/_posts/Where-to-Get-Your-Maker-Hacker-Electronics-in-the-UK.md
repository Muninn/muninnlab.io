---
title: Where to Get Your Maker/Hacker Electronics in the UK
date: 2018-05-19 13:00:35
tags:
 - hacker
 - maker
 - electronics
 - arduino
 - pi
---

If you're anything like me then programming probably isn't your only hobby and you try a fair few electronics projects at the same time. Two of my favourite Linux Magazines (Linux User & Developer and Linux Magazine) either have a regular Raspberry Pi sections OR they have tutorials about hacking Ardiuino's and related chips (ESP8266, ESP32 [Lua, MicroPython, C, etc]) in some way. There'S even a hugely popular UK magazine called MagPi which caters to people interested in hacking Raspberry Pi's or Pi Zero's.

This is my personally built up list of decent sites for electronic kits, components and hobbytronics:

## Chinese Suppliers

<img src="{% asset_path banggoodlogo.png %}" alt="BangGood Logo" />

## Gadgets and Electronics

### [BangGood.com](http://www.banggood.com)
I'd make sure you're not buying a trademarked/patented device suchh as the Teensey's which have official distributors but their ESP8266's are 10% the price of buying UK versions for the same equipment. For example at time of blogging here's [BangGood's NodeMCU ESP8266](https://www.banggood.com/NodeMcu-Lua-WIFI-Internet-Things-Development-Board-Based-ESP8266-CP2102-Wireless-Module-p-1097112.html?rmmds=search&stayold=1&cur_warehouse=CN) **selling for £3.38**. At Pimoroni they're selling for a **[whacking £12 before delivery](https://shop.pimoroni.com/collections/electronics/products/nodemcu-v2-lua-based-esp8266-development-kit)**. Did I mention Pimoroni charge delivery on top whereas BangGood are all free delivery? The only disadvantage being you have to wait a week instead of a few days. Check BangGood before anywhere else if you want DIY electronics kits, Arduinos, ESPX chips, etc. You really can make a MASSIVE saving. In fact BagGood are so much cheaper that for still less than what POimoroni charge you can get an ESP8266 chip with a tiny OLED screen embedded on the board for £7.66!!!

As a sweetener here's a few coupons they have going at the mo:
* [15% Off DIY Electronics](https://www.banggood.com/collection-5766.html?p=3D23198123540201612N&custlinkid=431549)
* [$53.99 for MINI TS80 Digital Soldering Iron Station](https://www.banggood.com/MINI-TS80-Digital-OLED-USB-Type-C-Programable-Soldering-Iron-Station-Solder-Tool-Built-in-STM32-Chip-p-1330060.html?utm_campaign=BGTS80&p=3D23198123540201612N&custlinkid=431553)
* [$8.49 for TTGO T-Display ESP32 CP2104 WiFi bluetooth Module](https://www.banggood.com/LILYGO-TTGO-T-Display-ESP32-CP2104-WiFi-bluetooth-Module-1_14-Inch-LCD-Development-Board-For-Arduino-p-1522925.html?utm_campaign=TTGO&p=3D23198123540201612N&custlinkid=431555)


### [GearBest](http://www.gearbest.com)
Tending less to the smaller components and more to gadgets its still worth checking this company out as they are one of the Chinese providers that offer massive discounts on expensive western imports.

## Raspbbery Pi/Arduino:

<a href="https://thepihut.com/"><img src="{% asset_path thepihut-logo.png %}" class="img-responsive" alt="The Pi Hut Logo" title="Sellers of Rapberry Pis and Beagle Bones" /></a>

### [The Pi Hut](https://thepihut.com/)
I have it from the horses mouth that if you want the latest and greatest of DFRobot's cool newest gadgets in the UK then these guys will have it first. I'm a HUGE fan and can easily spend hours just browsing their [Maker section](https://thepihut.com/collections/maker-store). I also have the afformentioned Beetle and μHex Controller from them.

<a href="https://www.modmypi.com/"><img src="{% asset_path modmypilogo.png %}" alt="ModMyPi Logo" class="img-responsive" title="Sellers of many maker kits and components" /></a>

### [ModMyPi](https://www.modmypi.com/)
Specialises mainly in Pi's and doesn't deviate much into the general hacker space like the Pi Hut which has its own maker area. ModMyPi can also be a bit expensive. Havoing said that they are oftenb the first to release things like Google's AIY or Raspberry Pi robots.

### [Pimoroni](https://shop.pimoroni.com/)
Has a really good selection of both official Pi and Arduino equipment as well as being one of the best Ada Fruit supplier in the UK. So if you're looking for an AdaBox (personally I think they're a rip off) then you can't go far wrong with Pimoroni. But be prepared - they charge heavily for what you get.

<blockquote>In fact I've compared prices before between the 3 major sites so far mentioned and found prices variances of £5-20 for a small product. But I've noticed in recent months a convergence of prices which concerns me as they are the major suppliers for makers at the moment. Am I being paranoid thinkning they're price fixing or just watching each other and price matching? Nowadays I find myself split between who charges the most for delivery as all the prices are exactly the same. Or I use one of the Chinese sellers or general electronics suppliers</blockquote>

## Maker/Hacker/Hobbyist Electronics

These are great for finding custom kits or something a bit different. These always fill a niche and I always have a browse now and again to find something new and cool to work on.

### [BitsBox](https://www.bitsbox.co.uk/)
They do lots of the most commonly used components but also have a great little selection of DIY kits for things like [amplifiers](https://www.bitsbox.co.uk/index.php?main_page=product_info&cPath=280_283&products_id=2020), [FM Bugs](https://www.bitsbox.co.uk/index.php?main_page=product_info&cPath=280_283&products_id=2830) and much more. What I also like is their great collection of easy to navigate [enclosures or project boxes](https://www.bitsbox.co.uk/index.php?main_page=index&cPath=185). Some coming with battery doors making them really handy.

<img src="{% asset_path rakit.png %}" class="img-responsive" alt="Rakits Logo" title="Maker of many great noise making electronic devices" style="background-color:black!important;padding:10px;" />

### [Rakits](https://www.rakits.co.uk/)
I love these guys so much! One of my favourites for DIY sytnth kits! They were one of my first forays into noise making projects and I discovered them via their ebay shop. I already have one oif every one of their kits but plan on buying extra of all of them to create breakout versions to put in project boxes I've bought from [BitsBox](https://www.bitsbox.co.uk/) to create my own little synth playground with sequencers - the works.

### [Hobbytronics](http://www.hobbytronics.co.uk)
Not a company I've used much for except the odd kit not available elsewhere. Always worth a check out though to make sure you're getting the best deal when checking other sites.

### [Conrad Electronics](https://www.conrad-electronic.co.uk)
Only recently discovered these guys and I'm already super impressed by their sheer selection of DIY project kits for electronics enthusisasts that covers Arduino's, Pi's and every other type of maker and hacker type out there. Check out their [kit section](https://www.conrad-electronic.co.uk/ce/en/category/SHOP_AREA_268309/Development-Kits-Development-Systems) to get an idea of what I mean.

## Electronic Components

I recommend these only if you know exactly what you want and have a code for an IC or transistor. As they have so many products its difficult to navigate the search to find individual products.

<a href="http://uk.farnell.com/"><img src="{% asset_path Farnell_element14.jpg %}" class="img-responsive" alt="Farnell Element 14 Logo" /></a>

### [Farnel/Element14](http://uk.farnell.com/)
These are one of the main official distributors of Raspberry Pi's as designated by teh RPi foundation. So you don't just get resistors, capacitors and whatnot here. You can get practically anything & everything yoiu want from these guys but the prices are a biut hefty unless you're buying in bulk. Its also a pain sifting through the sheer quantity of components for exactly what you need. Come prepared with exact component numbers and types.


<img src="{% asset_path CPC.png %}" class="img-responsive" alt="CPC Electronics Logo" />

### [CPC](http://cpc.farnell.com/)
Actually part of the Farnell group so you'd expect the prices to be the same. Except they're not. I think one is mainly for business while the other consumer but you can buy from either and its worth cross-checking both for pricing. Bare in mind they also charge differently for delivery.


## Missed Off?

**If you think there's any companies I've missed off the list be sure to email me dougbromley @ gmail etc.**

