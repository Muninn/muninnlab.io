---
title: My Career in Frameworks
date: 2020-12-23 10:25:16
categories: history
tags:
    - history
    - programming
    - php
    - javascript
---

I thought I'd list some of my framework experiences in terms of a timeline so potential employers and my current employer can get an overal p[icture of my experience in a new way. I plan to make this a posting style that incorporrates other elements such as Career in Languages or Career in Tech. Each one showing specifics of my career and what areas I learned or picked up at one poit in my career.

I think it could help current and future company's get an idea of how I may be rusty on a tech I learnt when they can put it into contect with the time I learnt it. It may be a useful tool to help me plan for the future as well. Many are the quotes regardign learnign from your history. So here goes my first page on frameworks. Both PHP and Javascript.

| Years  |       PHP Frameworks                                                             | JS Frameworks/Libs |
|--------|----------------------------------------------------------------------------------|--------------------|
|  05-06 | Procedural PHP4                                                                  |    Vanilla JS      |
|  06-08 | Procedural & Basic OO PHP4-5           5                                         |    Dojo JS/Ajax    |
|  08-10 | Cake PHP & OO PHP 5                                                              |    YUI & Prototype |
|  10-12 | [Kohana](https://kohanaframework.org) & [Django](https://www.djangoproject.com)  |    jQuery          | 
|  12-14 | [Yii 1.1](https://www.yiiframework.com/doc/guide/1.1/en) & Kohana                |    jQuery          |
|  12-15 | Virtuemart, Joomla, OpenCart, WooCommerce (e-commerce platforms)                 | jQuery & VanillaJS |
|  15-19 | Laravel, [F3](https://fatfreeframework.com/3.7/home), Lumen, [Silex](https://silex.symfony.com), procedural legacy PHP5 | jQuery |
|  19-20 | [Laravel](https://laravel.com), Legacy PHP5                                      | VueJS, jQuery & Vanilla JS |

The above list only includes projects compelted in a commercial context and doesn't include any open source or hobby technologiers I may have experimented with. If I'd included those the table would never get finished as behiond each framework has been a list of discarded try-outs with other frameworks in my own time especially during the period 05-18. More recently Laravel became the de-facto standard in most commercial projects.