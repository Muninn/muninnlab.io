---
title: about
date: 2017-09-06 09:40:26
categories: personal
tags:
  - personal
  - history
---

# Introduction
I’m a web developer, hacker, off-and-on guitarist, beard grower, cyclist,
runner, former jujutsuka and kenjutsuka.

As this is primarily a tech and coding blog I'll keep to those subjects.

# Career

* <a href="/My-Career-in-Frameworks/">My Career in Frameworks</a>
* My Career in Programming Languages
* My Career in Technologies
* My Career in Sys Admin

## Career Links
For more information on my professional career check out:

  * [LinkedIn](https://uk.linkedin.com/in/dougbromley)
  * [GitHub](http://github.com/OdinsHat)
  * [Gitlab](gitlab.com/Muninn)
  * [Stack Overflow](http://stackoverflow.com/users/288001/rustyfluff)

Other social media:

  * [Twitter](https://twitter.com/TinTopHat)

# Personal

For a more personal history of my technical background check out the  
[history page](/History/).



