---
title: pgp
date: 2017-09-26 07:16:50
tags:
    - security
    - netsec
    - cryptography
    - politics
---

# My PGP Key

If you wish to contact me with more privacy feel free to use the PGP key below to encrypt your message. I also have a more secure email account than my normal GMail accounts provided by protonmail: <a href="mailto:doug.bromley@protonmail.com">doug.bromley@protonmail.com</a>


```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBFnJ70sBCADJbQdQok3pfCTA8zZMG1oIrnCTz/YMgQMDnPI1HJ/bAMqfzIt1
qA27JGqnWRO4WdmkqRhbE86nJ54pUItRCY08q9LLjgPm9j7AodjWbWSKbzbf5fIx
+lvASw2x+pPrgxwu16aDDsoNDb3mnHkpQAencYpN8Y5kB0yJ0UvvCzt/p8OfaRpn
2nH4VrLH3WkBcJ3UTG49F9yXXz+CdDIKwIOXNVweT5mVBgrUliLV+Ob9xGYCUlQx
K9qJ5xoo+brAGhcCFzGPDMMhBcG1gYhGhuryies/CTN+6clzNgJJr60kPuJl7+in
8xHEKumy2xBooLXwU2WiZIpLbfQBqU23W2EHABEBAAG0KkRvdWcgQnJvbWxleSA8
ZG91Zy5icm9tbGV5QHByb3Rvbm1haWwuY29tPokBTgQTAQgAOBYhBNRcnoTqDX+O
Dw4rFt+oC9RTyZZYBQJZye9LAhsDBQsJCAcCBhUICQoLAgQWAgMBAh4BAheAAAoJ
EN+oC9RTyZZYutAH/1NkV4TKHDlkHjAvevMnSVmKio4dEzZcS1kbzOSa1i+7C67n
11qXPLIpFMMf0Nzhty5yDLsfiDsCyee2obICei2QuM+Fg/34pzab5dwFp0AaBnjx
ZF3EyHdGuexn6AEkZ0VlamOU+Ik01kondcE8KgpwklamHPaYHcJpAFE2W7NHBW66
r6dC7Betg1cRTPbPRiGCujWrBItFeRrNG29/pXoxiS8g82wNT1hbdcOUpIpavj+D
cEMDEwaRUk1+2N/b1+ZI1Pd/vwG9KF8SmJK+y/GTaxI7aTmhzyI3bPJlGRkVco96
bfv7qmd0XN+dTQ8CH3Qi/vacFEjzvUdSeaESs/y5AQ0EWcnvSwEIAL+OdpMRS9tK
6kMhXWrolRib5L/GELR2OaFUl0+gjLU6E1KjG8ELgbii60ao4dViscR4O0qYtfnc
MoLbZnDQNJiU5ePLK+FbH7/9PjmXNOgNqcGrSXAfUk5zsI4jTWfkM/4UnQsw0Npw
MKtUMLZbPyrAsz83fYOi5+WlMg005ozC3DqgghINrsTQi9zNyB+jcyL4nTZTlHhr
g8Idv/cfBKoPXdQIcnK5MTFu4eIE+2xq50M7EdACKqbbhJomXWEMcjulod1qeriL
znareHdEIhk03ThpHFYEYvFadZ1W74b9/53e2Tt0NfKYgCwi8U19ZscuGv9V4Bem
NiBwYkM3HwsAEQEAAYkBNgQYAQgAIBYhBNRcnoTqDX+ODw4rFt+oC9RTyZZYBQJZ
ye9LAhsMAAoJEN+oC9RTyZZY9IUH/R8HnCQ3+vIHVBNVOXN/m3bODsVaL1g4gq+F
pMflOxxfrACyvBCuvL9x9jzG6hAwGgdCiouA90RdjONIcCUSuTVN2yL1yuTfxidd
Y/1hBlM+rO6Axi9Z1+C7KlLPmOLqV8j7F14wQzWC8vLp5gntuwvinXcmgdSzpHUJ
EdKu3OqDuHqXwVKYkanK5DsFS8V+NVLU4lO94ZU7VpLDIF6Nfkro8ZTiAiI26mDO
Ma/BgtmsTkmeKUAFLI1O8zdWA5KEvPNt2IwV3IZ6fEaTxMIYiZWRHcUmf67uI5SE
cdffk43o+o6Yr8VOqkhfH4xb5cBZEP2To8eVa8wmv0f3To40v4s=
=Di5p
-----END PGP PUBLIC KEY BLOCK-----
```

## Server Locations

YUou can find/verify my PGP key at the following servers.

* [https://pgp.key-server.io/](https://pgp.key-server.io/)
* [hkp://keys.gnupg.net](hkp://keys.gnupg.net)
* [http://keyserver2.pgp.com](http://keyserver2.pgp.com)

## Article 8 [European Convention on Human Rights](https://en.wikipedia.org/wiki/European_Convention_on_Human_Rights#Article_8_.E2.80.93_privacy)
> A person has the right to a private and family life, his home and his correspondence