---
title: "History"
date: 2015-05-13 00:57:14
categories: personal
tags:
  - personal
  - history
---

## Early 1980's

Born, got access to my first PC at the age of 4 or 5. I think it was my dad's but he didn't get much time on it from what I remember. It was an Amstrad CPC 6128 - the exact one pictured below. I've held onto all these years. I started my programming back then in Amstrad BASIC.

{% asset_path "my-amstrad.jpg" %}

BASIC probably wasn't the best first language to experiment with but as a kid I was just desperate to hack away with anything and everything. I still  have that same Amstrad computer with me today pictured below. It works and here's a vieo to prove it:

<iframe width="560" height="315" title="Blog author programming an old CPC6128" src="https://www.youtube.com/embed/yivH2Ee62ZA" frameborder="0" allowfullscreen></iframe>

## Mid 90's

When I mentioned "hacking away at anything" that also included taking apart broken VHS players, shattered TV's, old car radios and various other scrapped and fly-tipped rubbish down a factory graveyard near my home. 
My mother was pretty patien at having a variety of cicuit boards and huge capacitors scattered around my bedroom and ocassionally enough cabling to rewire a neighbourhood left on the kitchen table.
Skipping Primary school which involved some daliance with Amigas lets hit High School. 
Poynton High School taught me very little other than the fact Research Machines were everywhere. At school I learnt how to make text **bold** in Word. At home I was learning to program in Pascal, C++ (as I'd heard of C and ++ must mean its WAY better!!). BASH scripting also got a look in as I started to experment with Linux. Especially Mandrake, Redhat, SuSE (I think).

## Late 90's
Towards the end of high school I'd got more involved with BASH scripting, C, C++ and Java. As well as the usual teenage tech enthusiasts desire to "hack/crack" everything he could. Joining the now defunct "CyberArmy" to hone my 7337 5killz (did I get that right? My memory is foggy) in 'hcking' FTP servers (actually just logging in and using the commands the server has). SSH, Telnet, etc were all played aorund with at this point. As well as several unsuccessful attempts at theming Windows (oh god!).

Using my 56K modem at the time I did manage to [wardial](https://en.wikipedia.org/wiki/War_dialing) about 30 telephone numbers thinking that my modems constant dialling, connecting, disconnecting must somehow make me the ultimate hacker of hackers.

<img src="{% asset_path 'Hackersposter.jpg' %}" alt="Hackers Poster" />

Thankfully my mum and step dad weren't home. Because for the next hour I had to field calls from angry people calling the house claiming "this number called me a minute ago!". I had to repeatedly play dumb to these irate calls until thankfully they died down before my parents got home. That put an end to my overt attempts to 'hack the planet'.

....to be continued, obviously. I'm 37 I've only reached my late teens here.


