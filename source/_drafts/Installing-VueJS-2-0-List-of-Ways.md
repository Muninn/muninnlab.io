---
title: Installing VueJS 2.0 - List of Ways
date: 2018-09-28 14:46:49
tags:
 - vuejs
 - javascript
 - javascript frameworks
 - SPA
---

![2022-03-15T191056](2022-03-15T191056.png)

## Installing VueJS 2.0

There's multiple ways you can go about installing JS I've come across during my training and I thought I'd put a list here not only for my future reference but also anyone else wondering how to start their journey. Though to be honest I think if you want to start your journey on learning VueJS you couldn't go wrong with one of the many VueJS courses I used myself as well as EggHead.io.

## Common to all

new Vue

### The CLI ?
Best not start off straight with the CLI as the site itself recommends:

<blockquote>
The CLI assumes prior knowledge of Node.js and the associated build tools. If you are new to Vue or front-end build tools, we strongly suggest going through the guide without any build tools before using the CLI.
</blockquote>

### CDN on JSFiddle

*Good for experimenting* JSFiddle now comes with a built in example and inclusion of the JSFiddle todo list using VueJS 2.0 which you can get by visiting the site and clicking the button or clicking [the boilerplate link](https://jsfiddle.net/boilerplate/vue/)

### Downloading the files to include

You can just download the latest files form the site to include in your site/app. Although I'd always recommend using a CDN version rather than a locally hosted edition so you can hopefully keep the latrest version.

### Using the CLI

When you finally feel more comfortable with VueJS and its build npm build methods then you can graduate to the CLI, especially when it comes to single page apps.