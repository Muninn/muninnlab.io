title: Coding Practice and Game Sites
date: 2015-01-15 14:32:08
tags: 
 - programming
 - coding
---

I wanted to put one of these together for a while simply because I love these type of sites and I think even veteran programmers can learn from many of them.

## CodeWars

<img src="{% asset_path "codewars-logo-long.png" %}" alt="Codewars Logo" />

This has to be one of my favourite of all time. I love how the style of the site is set out like a dojo and you earn your kyu gradings by passing kata. This is also one of the most comprehensives sites for languages available I've ever seen. As of now you can play/practice in:

* Javascript/NodeJS
* Python
* Ruby
* Java
* Clojure
* CoffeScript
* C#

Not only that but as you progress you may feel the desire to create your own challenges in one of those languages or one of the new languages that is preparing to launch on the site. And you guessed it all this comes with achievements, badges, etc for contributing and completing.

Similar to CodingBat challenges are split up into multiple types so you can hone your kills in certain areas:


## CheckIO
<img src="{% asset_path "checkio-logo.png" %}" alt="Check IO Logo" />

I'd say this site is aimed more to a younger audience simply because it has a more cartoony feel with RPG-style elements.

<img src="{% asset_path "checkio-home.png" %}" alt="Check IO Home" />

However, you'll find some of the more advanced problems a challenge even if you aren't in the target audience. The site is entirely dedicated to teaching **Python** as well so won't be suitable for anyone prefering a different language.

<img src="{% asset_path "checkio-challenges.png" %}" alt="Check Challenges" />

As with CodeWars it does provide the ability to create your own challenges so there's really nothing stopping people from creating challenges as hard as they want. The blog at CheckIO is suprisingly full of great information on PyGame, Python editor and various other coding topics.

## CodingBat

Very sparse layout but one of the oldest which caters to **Python** and **Java** programmers with challenges split into various categories such as String, List, Logic, etc so you can choose to hone your skills in a particular area.

I believe they recently introduced Code Badges which is a gamification of the site I hadn't seen when originally signing up. I think gamification of thee things is a great idea. Even just for me personally I love getting silly badges and achievements for little things. Maybe because I was indoctrinated by consoles and PC games whe I was yunger but if this is the result and it motivates some people to achieve more then its a good thing as far as I'm concerned

